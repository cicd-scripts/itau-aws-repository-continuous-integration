image: python:3.10

stages:
  - repository
  - post-validation
  - versioning

variables:
  SCRIPTS_REPO_LOCATION: cicd-scripts
  CODE_REPOS_LOCATION: https://$GITLAB_USER_LOGIN:$GITLAB_ACCESS_TOKEN@$CI_SERVER_HOST/$SCRIPTS_REPO_LOCATION
  COMMIT_MESSAGE_PATTERN: "Forçando alteração."
  TEMPLATE_NAME: "template.yml"
  CI_SCRIPTS_REPO: $CODE_REPOS_LOCATION/itau-aws-repository-continuous-integration.git
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository 
               -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN 
               -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

cache:
  key: $CI_JOB_NAME
  paths:
    - .m2/repository

.before_script: &before_script
  - set +e +o pipefail
  - pip install click

.install_python_and_pip: &install_python_and_pip
  - apt-get update
  - apt-get install -y --no-install-recommends python3.9
  - apt-get install -y --no-install-recommends python3-pip

default:
  before_script:
    - *before_script

get-scripts:
  stage: .pre
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
  script:
    - export SCRIPTS_DIR=$(mktemp -d)
    - git clone -q --depth 1 "$CI_SCRIPTS_REPO" artifacts
  artifacts:
    paths:
      - artifacts/scripts

validate-commit-message:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_VALIDATE_COMMIT_MESSAGE != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
  before_script:
    - *before_script
    - pip install regex
  script:
    - chmod +x artifacts/scripts/commit_message_validator.py
    - >
      python3 artifacts/scripts/commit_message_validator.py 
      --commit_message_pattern="$COMMIT_MESSAGE_PATTERN" 
      --commit_message="$CI_COMMIT_MESSAGE" || EXIT_CODE=$?
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

validate-infra-cloud-formation:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_VALIDATE_INFRA != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      exists:
        - infra/parameters-dev.json
        - infra/parameters-hom.json
        - infra/parameters-pro.json
        - infra/*.yml
      changes:
        - infra/*.yml
        - infra/*.json
  before_script:
    - *before_script
    - pip install pyyaml
    - pip install cfn-lint
  script:
    - chmod +x artifacts/scripts/cloud_formation_validator.py
    - >
      python3 artifacts/scripts/cloud_formation_validator.py 
      --template_name $TEMPLATE_NAME || EXIT_CODE=$?
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

coverage:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_COVERAGE != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      changes:
        - app/**/*
  script:
    - chmod +x artifacts/scripts/coverage_runner.py
    - python3 artifacts/scripts/coverage_runner.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

unit-test-java-maven:
  image: maven:3.8.6-jdk-11
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_UNIT_TEST != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      changes:
        - app/**/*
  before_script:
    - *install_python_and_pip
    - *before_script
  script:
    - chmod +x artifacts/scripts/java_maven_unit_tester.py
    - python3 artifacts/scripts/java_maven_unit_tester.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

unit-test-python:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_UNIT_TEST != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      exists:
        - app/*.py
      changes:
        - app/**/*
  before_script:
    - *before_script
    - pip install pytest
  script:
    - chmod +x artifacts/scripts/python_unit_tester.py
    - python3 artifacts/scripts/python_unit_tester.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

integrated-test:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_INTEGRATED_TEST != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      changes:
        - app/**/*
  script:
    - chmod +x artifacts/scripts/integration_tester.py
    - python3 artifacts/scripts/integration_tester.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

mutation-test:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_MUTATION_TEST != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      changes:
        - app/**/*
  script:
    - chmod +x artifacts/scripts/mutation_tester.py
    - python3 artifacts/scripts/mutation_tester.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

build-java-maven:
  image: maven:3.8.6-jdk-11
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_BUILD != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      exists:
        - app/pom.xml
      changes:
        - app/**/*
  before_script:
    - *install_python_and_pip
    - *before_script
  script:
    - chmod +x artifacts/scripts/java_maven_builder.py
    - python3 artifacts/scripts/java_maven_builder.py || EXIT_CODE=$?
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

build-java-gradle:
  image: maven:3.8.6-jdk-11
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_BUILD != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      exists:
        - app/pom.xml # todo
      changes:
        - app/**/*
  before_script:
    - *install_python_and_pip
    - *before_script
  script:
    - chmod +x artifacts/scripts/java_gradle_builder.py
    - python3 artifacts/scripts/java_gradle_builder.py || EXIT_CODE=$?
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

build-python:
  stage: repository
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_BUILD != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      exists:
        - app/*.py
      changes:
        - app/**/*
  before_script:
    - *before_script
    - pip install flake8
  script:
    - chmod +x artifacts/scripts/python_builder.py
    - python3 artifacts/scripts/python_builder.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

validate-format:
  stage: post-validation
  interruptible: true
  rules:
    - if: ($IGNORE_ALL != "true" &&
        $IGNORE_VALIDATE_FORMAT != "true" &&
        $CI_COMMIT_TITLE !~ /(?i)^(WIP|Draft)\w*$/ &&
        $CI_PIPELINE_SOURCE == "merge_request_event" &&
        $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^(feature-\w+|hotfix-\w+)$/ &&
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(develop|release|hotfix|master)$/)
      changes:
        - app/**/*
  script:
    #   - referência: https://github.com/plume-lib/run-google-java-format
    - chmod +x artifacts/scripts/format_validator.py
    - python3 artifacts/scripts/format_validator.py || EXIT_CODE=$? # todo
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79

tag-branch:
  stage: versioning
  interruptible: true
  rules:
    - if: $CI_BUILD_REF_NAME == "main" && $CI_PIPELINE_SOURCE == "push"
  before_script:
    - *before_script
    - pip install semver
    - pip install regex
    - pip install python-gitlab
  script:
    - chmod +x artifacts/scripts/tagger.py
    - python3 artifacts/scripts/tagger.py $GITLAB_ACCESS_TOKEN $CI_SERVER_URL $CI_PROJECT_ID $CI_JOB_ID || EXIT_CODE=$?
    - exit $EXIT_CODE
  allow_failure:
    exit_codes: 79
