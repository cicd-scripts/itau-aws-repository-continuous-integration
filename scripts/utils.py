import json
import subprocess
import sys
from dataclasses import dataclass

import click

OK = "OK"
NOK = "NOK"
WARN = "WARN"
RESULT = {"OK": 0, "NOK": 1, "WARN": 79}


@dataclass
class SubprocessResult:
    return_code: int
    stdout: str
    stderr: str


def exit_job(result: str):
    sys.exit(RESULT.get(result))


def json_to_dict(file_name: str) -> dict:
    with open(file_name) as file:
        return json.load(file)


def run_subprocess(command: str) -> SubprocessResult:
    result = subprocess.Popen(
        command.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )

    stdout, stderr = result.communicate()
    click.echo(stdout)
    click.echo(stderr)

    return SubprocessResult(result.returncode, stdout, stderr)
