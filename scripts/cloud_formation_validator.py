import json
from dataclasses import dataclass

import click
import yaml

from utils import exit_job, OK, NOK, WARN, json_to_dict, run_subprocess

AWS_CLOUD_FORMATION_ERROR_MAPPING: dict = {
    OK: [0],
    NOK: [2, 6, 10, 14],
    WARN: [4, 8, 12],
}


def _get_parameter_result(parameters_results: dict) -> str:
    results = [
        results.get("result")
        for data in parameters_results.get("parameters")
        for results in data.values()
    ]

    return _get_result(results)


def _get_final_result(final_result: dict) -> str:
    results = [results.get("result") for results in final_result.values()]

    return _get_result(results)


def _get_result(results: list[str]) -> str:
    if all(result == OK for result in results):
        return OK
    elif any(result == NOK for result in results):
        return NOK
    elif any(result == WARN for result in results):
        return WARN
    else:
        return NOK


def _cfn_lint_result_to_json(stdout: str) -> dict:
    try:
        return {
            f"Error {id_ + 1}": {
                "error_code": error_info[0].split()[0],
                "message": " ".join(error_info[0].split()[1:]),
                "location": error_info[1],
            }
            for id_, error_info in enumerate(
                list(
                    zip(
                        *(iter([line for line in stdout.split("\n") if line]),)
                        * 2
                    )
                )
            )
        }
    except Exception as e:
        click.echo(f"Erro na conversão do log para json: {str(e)}")
        click.echo(stdout)
        return {}


def _validate_cloud_formation(template_name: str) -> str:
    result = run_subprocess(f"cfn-lint infra/{template_name}")

    result_as_json: dict = _cfn_lint_result_to_json(result.stdout)

    click.echo(
        json.dumps(
            result_as_json,
            indent=4,
            ensure_ascii=False,
        )
    )

    return [
        k[0]
        for k in AWS_CLOUD_FORMATION_ERROR_MAPPING.items()
        if result.return_code in k[1]
    ][0]


def _compose_result(
    itau_validation: str, cloud_formation_validation: str
) -> str:
    all_validation = [itau_validation, cloud_formation_validation]

    return _get_result(all_validation)


@click.command()
@click.option("--template_name", help="Nome do template.")
def validate_infra(template_name: str):
    try:
        itau_validation = _validate_itau_cloud_formation(template_name)
        cloud_formation_validation = _validate_cloud_formation(template_name)

        final_result = _compose_result(
            itau_validation, cloud_formation_validation
        )

        exit_job(final_result)
    except Exception as e:
        click.echo(f"Erro na execução do job: {str(e)}")
        exit_job(NOK)


@dataclass
class CloudFormationData:
    parameters: dict[str, dict]
    template: dict


def _build_cloud_formation_data(template_name: str) -> CloudFormationData:
    return CloudFormationData(
        parameters={
            "dev": _get_cloud_formation_parameters_dev(),
            "hom": _get_cloud_formation_parameters_hom(),
            "pro": _get_cloud_formation_parameters_pro(),
        },
        template=_get_cloud_formation_template(template_name),
    )


def _get_results_by_environment(results: list[dict]) -> dict:
    return {
        key: {"parameters": [result.get(key) for result in results]}
        for key in set().union(*results)
    }


def _set_results_by_environment(results_by_environment: dict):
    results_by_environment["dev"]["result"] = _get_parameter_result(
        results_by_environment["dev"]
    )
    results_by_environment["hom"]["result"] = _get_parameter_result(
        results_by_environment["hom"]
    )
    results_by_environment["pro"]["result"] = _get_parameter_result(
        results_by_environment["pro"]
    )


def _validate_itau_cloud_formation(template_name: str) -> str:
    cloud_formation_data: CloudFormationData = _build_cloud_formation_data(
        template_name
    )
    results: list[dict] = []

    for parameter in cloud_formation_data.template.get("Parameters").items():
        results.append(
            _validate_parameter_on_environments(
                parameter, cloud_formation_data
            )
        )

    results_by_environment: dict = _get_results_by_environment(results)
    _set_results_by_environment(results_by_environment)

    click.echo(
        json.dumps(
            {
                key: value
                for key, value in sorted(results_by_environment.items())
            },
            indent=4,
            ensure_ascii=False,
        )
    )

    return _get_final_result(results_by_environment)


def _validate_parameter_on_environments(
    parameter: tuple[str, dict], cloud_formation_data: CloudFormationData
) -> dict:
    parameter_name: str = parameter[0]
    result: dict = {"dev": {}, "hom": {}, "pro": {}}

    click.echo("-----------------------------")
    click.echo(f"Validando parâmetro {parameter_name}...")

    result["dev"][parameter_name]: dict = _validate_parameter(
        "dev", parameter, cloud_formation_data
    )
    result["hom"][parameter_name]: dict = _validate_parameter(
        "hom", parameter, cloud_formation_data
    )
    result["pro"][parameter_name]: dict = _validate_parameter(
        "pro", parameter, cloud_formation_data
    )

    return result


def _validate_parameter(
    environment: str,
    parameter: tuple[str, dict],
    cloud_formation_data: CloudFormationData,
) -> dict:
    click.echo(f"Validando ambiente de {environment}...")

    parameter_name: str = parameter[0]
    parameter_definition: dict = parameter[1]
    parameters: dict = cloud_formation_data.parameters.get(environment)
    has_default_value: bool = "Default" in parameter_definition.keys()
    parameter_type: type = _get_parameter_type(
        parameter_definition.get("Type")
    )
    result: str
    message: str

    if parameter_name not in parameters.keys():
        if not has_default_value:
            result = NOK
            message = (
                f"Parâmetro '{parameter_name}' não existe nas "
                f"variáveis e não possui valor default."
            )
        else:
            result = WARN
            message = (
                f"Parâmetro '{parameter_name}' não existe nas "
                f"variáveis, porém possui valor default."
            )
    else:
        if not isinstance(parameters.get(parameter_name), parameter_type):
            result = NOK
            message = (
                f"O valor definido nos parâmetros é do tipo "
                f"{type(parameters.get(parameter_name)).__name__} e deveria "
                f"ser do tipo {parameter_type.__name__}."
            )
        else:
            result = OK
            message = ""

    return {"result": result, "message": message}


def _get_cloud_formation_template(template_name: str) -> dict:
    return _yml_to_dict(f"infra/{template_name}")


def _get_cloud_formation_parameters_dev() -> dict:
    return _get_cloud_formation_parameters(env="dev")


def _get_cloud_formation_parameters_hom() -> dict:
    return _get_cloud_formation_parameters(env="hom")


def _get_cloud_formation_parameters_pro() -> dict:
    return _get_cloud_formation_parameters(env="pro")


def _get_cloud_formation_parameters(env: str) -> dict:
    file_name = f"infra/parameters-{env}.json"

    return json_to_dict(file_name)


def _get_parameter_type(cloud_formation_type: str) -> type:
    if cloud_formation_type == "String":
        return str
    elif cloud_formation_type == "Number":
        return int
    else:
        return str


def _yml_to_dict(file_name: str) -> dict:
    with open(file_name) as file:
        return yaml.safe_load(file)


if __name__ == "__main__":
    validate_infra()
