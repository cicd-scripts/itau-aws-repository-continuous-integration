import click

from utils import exit_job, NOK, OK, run_subprocess


@click.command
def run_unit_tests():
    try:
        result = run_subprocess("pytest app")

        exit_job(OK if result.return_code == 0 else NOK)
    except Exception as e:
        click.echo(f"Erro na execução do job: {str(e)}")
        exit_job(NOK)


if __name__ == "__main__":
    run_unit_tests()
