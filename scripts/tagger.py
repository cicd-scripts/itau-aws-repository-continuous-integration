import json
from typing import List

import click
import regex
from gitlab import Gitlab
from gitlab.v4.objects import (
    Project,
    ProjectJob,
    ProjectMergeRequest,
    ProjectTag,
)
from semver import VersionInfo

from utils import OK, NOK, WARN

GITLAB: Gitlab
GITLAB_PROJECT: Project
MERGE_REQUEST_DESCRIPTION_PATTERN = (
    "^\\[\\d+\\] Entrega de histórias (funcionais|técnicas) \\[\\s*("
    "?P<jira_issue_pattern>(?<!([aA-zZ])-)[aA-zZ]+-\\d+)+?((?:(\\s*)?,(\\s*)?"
    "((?P>jira_issue_pattern)+?)))*\\s*\\]$"
)


def _buscar_job_info(job_id: str) -> ProjectJob:
    click.echo(f"Buscando as informações do Job {job_id}.")

    return GITLAB_PROJECT.jobs.get(job_id)


def _buscar_merge_request_info(merge_request_iid: str) -> ProjectMergeRequest:
    click.echo(
        f"Buscando as informações do Merge Request {merge_request_iid}."
    )

    return GITLAB_PROJECT.mergerequests.get(merge_request_iid)


# noinspection PyTypeChecker
def _buscar_tags_info() -> List[ProjectTag]:
    click.echo("Buscando as tags do repositório.")

    return GITLAB_PROJECT.tags.list()


def _criar_nova_tag(tag_name: str, ref: str, message: str):
    click.echo(
        f"Criando a tag {tag_name} na branch {ref} com a descrição {message}."
    )

    return GITLAB_PROJECT.tags.create(
        {"tag_name": tag_name, "ref": ref, "message": message}
    )


# noinspection PyTypeChecker
@click.command()
@click.option(
    "--gitlab_token",
    help="Token do GitLab para ser usado nas chamadas das APIs.",
)
@click.option(
    "--base_url",
    help="URL da instância do GitLab onde está sendo rodado o script.",
)
@click.option(
    "--project_id", help="ID do projeto que o CI/CD está sendo executado,"
)
@click.option("--job_id", help="ID do Job que está sendo executado.")
def tag_repository(
    gitlab_token: str, base_url: str, project_id: str, job_id: str
) -> str:
    try:
        click.echo(f"Executando job com a rota {base_url}")
        global GITLAB
        global GITLAB_PROJECT

        GITLAB = Gitlab(
            url=base_url,
            private_token=gitlab_token,
        )
        GITLAB_PROJECT = GITLAB.projects.get(project_id)

        job_info = _buscar_job_info(job_id)

        click.echo("Job Info:")
        click.echo(
            json.dumps(job_info.attributes, indent=4, ensure_ascii=False)
        )

        merge_request_iid = _identificar_merge_request_iid(job_info)

        merge_request_info = _buscar_merge_request_info(
            merge_request_iid=merge_request_iid
        )

        click.echo("Merge Request Info:")
        click.echo(
            json.dumps(
                merge_request_info.attributes, indent=4, ensure_ascii=False
            )
        )

        if _merge_candidato_para_producao(merge_request_info):
            descricao = merge_request_info.attributes.get("description")
            source_branch = merge_request_info.attributes.get("source_branch")
            target_branch = merge_request_info.attributes.get("target_branch")

            tags_info = _buscar_tags_info()
            tags = _extrair_tags(tags_info)

            click.echo(f"Tags do repositório: {tags}")
            nova_tag = _definir_nova_tag(tags, source_branch)

            _criar_nova_tag(
                tag_name=nova_tag,
                ref=target_branch,
                message=descricao,
            )

            return OK
        else:
            click.echo(
                "Nenhuma tag será gerada pois o Merge Request não é "
                "candidato para implantação em Produção."
            )
            return WARN
    except Exception as e:
        click.echo(f"Erro na execução do job: {str(e)}")
        return NOK


def _get_base_url(kwargs: dict):
    return f"{kwargs.get('base_url')}/projects/{kwargs.get('project_id')}/"


def _identificar_merge_request_iid(job_info: ProjectJob) -> str:
    """

    :param job_info: um dict com o response da chamada da API
    /api/v4/projects/{id_project}/jobs/{id_job}
    :return: uma string contendo o id do merge request referente ao merge
    efetivado ao qual o job está sendo processado; a mensagem de commit de
    um evento de merge request efetivado tem o padrão de sempre colocar o id do
    merge request no final da mensagem, prefixada com um "!"
    """
    commit_message: str = job_info.attributes.get("commit").get("message")

    return commit_message.rsplit("!")[-1]


def _merge_candidato_para_producao(
    merge_request_info: ProjectMergeRequest,
) -> bool:
    """

    :param merge_request_info: um dict com o response da chamada da API
    /api/v4/projects/{id_project}/merge_requests/{iid_merge_request}
    :return: um boolean com a validação se a descrição do merge request
    possui o padrão de template de entrega de histórias
    """
    descricao = merge_request_info.attributes.get("description")

    return bool(regex.match(MERGE_REQUEST_DESCRIPTION_PATTERN, descricao))


def _extrair_historias_da_descricao(descricao: str) -> List[str]:
    """

    :param descricao: descrição do merge request
    :return: uma lista com as histórias listadas na descrição do merge
    request; a descrição possui as histórias dentro de colchetes [] e para
    extraí-las da descrição, é necessário pegar o segundo grupo de palavras
    entre colchetes, dado que o template inicia sempre com [1].
    """
    historias = regex.findall("\\[(.*?)\\]", descricao)[1]
    historias = historias.split(",")

    return [historia.strip() for historia in historias]


# noinspection PyUnresolvedReferences
def _extrair_tags(tags_info: List[dict]) -> List[str]:
    return [tag_info.name for tag_info in tags_info]


def _definir_nova_tag(tags: List[str], source_branch: str) -> str:
    tag_produtiva_mais_recente = _buscar_tag_produtiva_mais_recente(tags)
    part = _definir_part_para_nova_versao(source_branch)
    proxima_tag_produtiva = tag_produtiva_mais_recente.next_version(part=part)

    if _ja_existe_release_candidate(str(proxima_tag_produtiva), tags):
        tag_mais_recente = _buscar_tag_mais_recente(tags)

        return str(tag_mais_recente.next_version(part="prerelease"))
    else:
        return f"{str(proxima_tag_produtiva)}-rc1"


def _definir_part_para_nova_versao(source_branch: str) -> str:
    return "patch" if ("hotfix" or "hot-fix") in source_branch else "minor"


def _ja_existe_release_candidate(tag_produtiva: str, tags: List[str]) -> bool:
    return any(tag_produtiva in tag_ for tag_ in tags)


def _buscar_tag_produtiva_mais_recente(tags: List[str]) -> VersionInfo:
    ultima_tag_produtiva = max(
        _listar_tags_produtivas(tags), key=VersionInfo.parse
    )

    return VersionInfo.parse(ultima_tag_produtiva)


def _buscar_tag_mais_recente(tags: List[str]) -> VersionInfo:
    ultima_tag_produtiva = max(tags, key=VersionInfo.parse)

    return VersionInfo.parse(ultima_tag_produtiva)


def _listar_tags_produtivas(tags: List[str]) -> List[str]:
    return [_tag for _tag in tags if "rc" not in _tag]


if __name__ == "__main__":

    tag_repository()
