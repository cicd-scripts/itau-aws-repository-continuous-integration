import click

from scripts.utils import exit_job, NOK, OK, run_subprocess


@click.command()
def build():
    try:
        result = run_subprocess("mvn clean compile -f app")

        exit_job(OK if result.return_code == 0 else NOK)
    except Exception as e:
        click.echo(f"Erro na execução do job: {str(e)}")
        exit_job(NOK)


if __name__ == "__main__":
    build()
