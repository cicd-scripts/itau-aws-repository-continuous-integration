import click
import regex

from utils import OK, NOK, exit_job


@click.command()
@click.option("--commit_message_pattern", help="Padrão de mensagem do commit.")
@click.option("--commit_message", help="Mensagem do commit.")
def validate_commit_message(commit_message_pattern: str, commit_message: str):
    try:
        if _is_message_valid(commit_message_pattern, commit_message):
            click.echo("Mensagem de commit dentro do padrão determinado.")
            exit_job(OK)
        else:
            click.echo(
                f"Mensagem de commit fora do padrão, a mensagem deveria "
                f'estar no padrão "{commit_message_pattern}" e a mensagem é'
                f' "{commit_message}"'
            )
            exit_job(NOK)
    except Exception as e:
        click.echo(f"Erro na execução do job: {str(e)}")
        exit_job(NOK)


def _is_message_valid(
    commit_message_pattern: str, commit_message: str
) -> bool:
    return bool(
        regex.match(
            commit_message_pattern,
            commit_message,
        )
    )


if __name__ == "__main__":
    validate_commit_message()
