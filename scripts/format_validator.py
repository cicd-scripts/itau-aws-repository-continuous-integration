import click

from utils import exit_job, WARN, NOK


@click.command()
def validate_format():
    try:
        click.echo("Função não implementada.")
        exit_job(WARN)
    except Exception as e:
        click.echo(f"Erro na execução do job: {str(e)}")
        exit_job(NOK)


if __name__ == "__main__":
    validate_format()
